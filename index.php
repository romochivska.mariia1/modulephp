<?php
$student = [
    'id' => null,
    'full_name' => null,
    'course' => null,
    'major' => null,
];
$students = [
    [
        'id' => 1,
        'full_name' => 'John Smith',
        'course' => 2,
        'major' => 'Computer Science',
    ],
    [
        'id' => 2,
        'full_name' => 'Mary Johnson',
        'course' => 3,
        'major' => 'Economics',
    ],
    [
        'id' => 3,
        'full_name' => 'Michael Brown',
        'course' => 1,
        'major' => 'Mathematics',
    ],
    [
        'id' => 4,
        'full_name' => 'Emily Davis',
        'course' => 4,
        'major' => 'Physics',
    ],
    [
        'id' => 5,
        'full_name' => 'Robert Wilson',
        'course' => 2,
        'major' => 'Chemistry',
    ],
];

function saveToCSV($data) {
    $csvFile = fopen('data.csv', 'a');

    foreach ($data as $student) {
        fputcsv($csvFile, $student);
    }

    fclose($csvFile);
}

function deleteStudent(&$students, $index) {
    if (isset($students[$index])) {
        unset($students[$index]);
        $students = array_values($students); // Перенумеровуємо масив
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['save'])) {
        $id = $_POST['id'];
        $full_name = $_POST['full_name'];
        $course = $_POST['course'];
        $major = $_POST['major'];

        $new_entry = [
            'id' => $id,
            'full_name' => $full_name,
            'course' => $course,
            'major' => $major,
        ];

        $students[] = $new_entry;

        saveToCSV([$new_entry]);

        echo 'Дані збережено успішно.';
    } elseif (isset($_POST['delete_index'])) {
        $index = $_POST['delete_index'];
        deleteStudent($students, $index);
    }
}
?>

<table border="1">
    <thead>
    <th>Id</th>
    <th>ПІБ</th>
    <th>Курс</th>
    <th>Спеціальність</th>
    <th>Дія</th>
    </thead>
    <?php foreach ($students as $key => $student): ?>
        <tr>
            <td><?= $student['id']; ?></td>
            <td><?= $student['full_name']; ?></td>
            <td><?= $student['course']; ?></td>
            <td><?= $student['major']; ?></td>
            <td>
                <form method="POST" action="">
                    <input type="hidden" name="delete_index" value="<?= $key; ?>">
                    <button type="submit">Видалити</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Форма для збереження даних</title>
</head>
<body>
<h1>Форма для збереження даних</h1>
<form method="POST" action="">
    <label for="id">ID:</label>
    <input type="text" id="id" name="id" placeholder="Введіть id" required><br>

    <label for="full_name">ПІБ:</label>
    <input type="text" id="full_name" name="full_name" placeholder="Введіть ПІБ" required><br>

    <label for="course">Курс:</label>
    <input type="text" id="course" name="course" placeholder="Введіть курс" required><br>

    <label for="major">Спеціальність:</label>
    <input type="text" id="major" name="major" placeholder="Введіть спеціальність" required><br>

    <input type="submit" name="save" value="Зберегти дані">
</form>
</body>
</html>